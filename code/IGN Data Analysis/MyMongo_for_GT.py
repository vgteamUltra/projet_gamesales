
# coding: utf-8

# In[56]:

from pymongo import *


class MongoManagement():
    def MongoConnect(self, uri="mongodb://localhost:27017"):
        # self : mot clef qui permet d'aller chercher les variables dans l'instance (evite d'avoir a appeler this.variable comme en java)
        client=MongoClient(uri)
        return client

    def MongoConnectDB(self,client=MongoClient(),dbname="baseParDefaut"):
        if client:
            dataBase=client[dbname]
        else:
            return -1
        return dataBase

    
    def MongoConnectDBColl(self,client=MongoClient(),dbname="baseParDefaut",cname="collectionParDefaut"):
        if client:
            dataBaseColl=client[dbname][cname]
        else:
            return -1
        return dataBaseColl
    
    def oneUpsert(self,coll,keysRef,Dic): # Attributs : NomCollection, ClefsReference, Dico a upsert (ajouter ou mettre à jour)
        if coll and keysRef and Dic:
            if type(keysRef)==str: # Transformation necessaire pour traiter une liste ou une clef (str --> transformée en liste)
                keysRef=[keysRef]
            keys=Dic.keys()
            keysVal=list(set(keys)-set(keysRef))   # On recupere la liste des clefs a intégrer ou mettre a jour

            dicRef={}  # Dictionnaire des references (identification --> nouvelle ligne ou mise à jour)
            dicVal={}
            for kr in keysRef:
                dicRef[kr]=Dic[kr]
            for kv in keysVal:
                dicVal[kv]=Dic[kv]

            coll.update_one(dicRef, {'$set': dicVal}, upsert=True)
        else:
            print "Attribut(s) manquants ! bulkUpsert(NomCollection, ClefsRef, listDic)"

    def bulkUpsert(self,coll,keysRef,listDic): # Attributs : NomCollection, ClefsReference, Liste de dico a upsert (ajotuer ou mettre à jour)
        if coll and keysRef and listDic:
            if type(keysRef)==str: # Transformation necessaire pour traiter une liste ou une clef (str --> transformée en liste)
                keysRef=[keysRef]
            keys=listDic[0].keys()
            keysVal=list(set(keys)-set(keysRef))   # On recupere la liste des clefs a intégrer ou mettre a jour

            for elem in listDic:
                dicRef={}  # Dictionnaire des references (identification --> nouvelle ligne ou mise à jour)
                dicVal={}
                for kr in keysRef:
                    dicRef[kr]=elem[kr]
                for kv in keysVal:
                    dicVal[kv]=elem[kv]

                coll.update_one(dicRef, {'$set': dicVal}, upsert=True)
        else:
            print "Attribut(s) manquants ! bulkUpsert(NomCollection, ClefsRef, listDic)"
            
    def oneInsert(self,coll,Dic):
        coll.insert_one(Dic)

#uri= "mongodb://84.39.39.108:27017"
#myMongo = MongoManagement()
#MongoClient = myMongo.MongoConnect(uri)

#dataName='citibike'
#collName='weather_H'

##DBconnected = myMongo.MongoConnectDB(MongoClient, dataName)
#Cconnected = myMongo.MongoConnectDBColl(MongoClient, dataName, collName) 
   
#myMongo.bulkUpsert(Cconnected, "name", listDic)

print 'ok'


