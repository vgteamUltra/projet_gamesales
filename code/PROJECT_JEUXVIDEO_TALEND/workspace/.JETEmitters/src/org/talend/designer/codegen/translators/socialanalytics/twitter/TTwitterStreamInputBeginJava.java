package org.talend.designer.codegen.translators.socialanalytics.twitter;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.utils.NodeUtil;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.HashMap;

public class TTwitterStreamInputBeginJava
{
  protected static String nl;
  public static synchronized TTwitterStreamInputBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TTwitterStreamInputBeginJava result = new TTwitterStreamInputBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t\t/**" + NL + "\t\t * Talend Bridge Logger" + NL + "\t\t * @see http://gabrielebaldassarre.com" + NL + "\t\t */" + NL + "\t\t" + NL + "\t\tclass TalendTwitterLogger_";
  protected final String TEXT_2 = " implements java.util.Observer{" + NL + "" + NL + "\t\t\t   public void update(java.util.Observable obs, Object obj)" + NL + "\t\t\t   {" + NL + "\t\t\t   ";
  protected final String TEXT_3 = " log = (";
  protected final String TEXT_4 = ")obj;\t\t\t   " + NL + "\t\t\t   ";
  protected final String TEXT_5 = NL + "\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_6 = ".addMessage(\"tWarn\", \"";
  protected final String TEXT_7 = "\", 4, log.getMessage(), 210);" + NL + "\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_8 = "Process(globalMap);" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_9 = NL + "\t\t\t   \tresumeUtil.addLog(log.getCategory(), \"NODE:";
  protected final String TEXT_10 = "\", \"\", log.getThreadId() + \"\", log.getSeverity(),\"\", log.getMessage(),\"\", \"\");" + NL + "\t\t\t\t   System.err.println(log.getMessage());" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\tTalendTwitterLogger_";
  protected final String TEXT_11 = " twitterLogger_";
  protected final String TEXT_12 = " = new TalendTwitterLogger_";
  protected final String TEXT_13 = "();" + NL + "" + NL + "\t\tint nb_line_";
  protected final String TEXT_14 = " = 0;" + NL + "\t\t" + NL + " \t\t//Build Filter" + NL + " \t\t" + NL + " \t\tjava.util.List<String> languageList_";
  protected final String TEXT_15 = " = new java.util.ArrayList<String>();" + NL + "\t\tjava.util.List<String> keywordList_";
  protected final String TEXT_16 = " = new java.util.ArrayList<String>();" + NL + "\t\tjava.util.List<String> excludeList_";
  protected final String TEXT_17 = " = new java.util.ArrayList<String>();" + NL + "\t\tjava.util.List<String> fromList_";
  protected final String TEXT_18 = " = new java.util.ArrayList<String>();" + NL + "\t\tjava.util.List<String> atList_";
  protected final String TEXT_19 = " = new java.util.ArrayList<String>();" + NL + "\t\t" + NL + "\t\t";
  protected final String TEXT_20 = NL + "\t\t" + NL + "\t\tString[] languages_";
  protected final String TEXT_21 = " = languageList_";
  protected final String TEXT_22 = ".toArray(new String[languageList_";
  protected final String TEXT_23 = ".size()]);" + NL + "\t\tString[] keywords_";
  protected final String TEXT_24 = " = keywordList_";
  protected final String TEXT_25 = ".toArray(new String[keywordList_";
  protected final String TEXT_26 = ".size()]);" + NL + "\t\tString[] exclude_";
  protected final String TEXT_27 = " = excludeList_";
  protected final String TEXT_28 = ".toArray(new String[excludeList_";
  protected final String TEXT_29 = ".size()]);" + NL + "\t\tString[] at_";
  protected final String TEXT_30 = " = atList_";
  protected final String TEXT_31 = ".toArray(new String[atList_";
  protected final String TEXT_32 = ".size()]);" + NL + "\t\tString[] from_";
  protected final String TEXT_33 = " = fromList_";
  protected final String TEXT_34 = ".toArray(new String[fromList_";
  protected final String TEXT_35 = ".size()]);" + NL + "\t\t" + NL + "\t\ttwitter4j.FilterQuery fq_";
  protected final String TEXT_36 = " = new twitter4j.FilterQuery();" + NL + "\t\tfq_";
  protected final String TEXT_37 = ".track(keywords_";
  protected final String TEXT_38 = ").language(languages_";
  protected final String TEXT_39 = ");" + NL + "" + NL + "\t\t/**" + NL + "\t\t * TOS-bridge data structures" + NL + "\t\t * @see http://gabrielebaldassarre.com" + NL + "\t\t */" + NL + "\t\t " + NL + "\t\t// data model initialization      " + NL + "\t\t";
  protected final String TEXT_40 = " tcontroller_";
  protected final String TEXT_41 = " = ";
  protected final String TEXT_42 = ".getInstance();" + NL + "\t\t";
  protected final String TEXT_43 = " tmodel_";
  protected final String TEXT_44 = " = tcontroller_";
  protected final String TEXT_45 = ".getModel(new ";
  protected final String TEXT_46 = "(globalMap));" + NL + "" + NL + "\t\t// flows factory interface" + NL + "\t\t";
  protected final String TEXT_47 = " tablefactory_";
  protected final String TEXT_48 = " = tmodel_";
  protected final String TEXT_49 = ".getFlowFactory();" + NL + "\t\t        " + NL + "\t\t// init needed data flows" + NL + "\t\t";
  protected final String TEXT_50 = " tweets_";
  protected final String TEXT_51 = " = tablefactory_";
  protected final String TEXT_52 = ".newFlow(\"Tweets\", ";
  protected final String TEXT_53 = ", false);" + NL + "\t\t" + NL + "\t\t// Prepare columns for tweets details data flow";
  protected final String TEXT_54 = NL + "\t\t// Prepare visitors" + NL + "" + NL + "\t\t";
  protected final String TEXT_55 = " trCrawl_";
  protected final String TEXT_56 = " = new ";
  protected final String TEXT_57 = "((twitter4j.TwitterBase)globalMap.get(\"twitter_";
  protected final String TEXT_58 = "\"), ";
  protected final String TEXT_59 = ", ";
  protected final String TEXT_60 = ", ";
  protected final String TEXT_61 = ".";
  protected final String TEXT_62 = ");" + NL + "\t\ttrCrawl_";
  protected final String TEXT_63 = ".addObserver(twitterLogger_";
  protected final String TEXT_64 = ");" + NL + "\t\t" + NL + "\t\t//add Listener" + NL + "\t\t";
  protected final String TEXT_65 = " statusListener_";
  protected final String TEXT_66 = " = new ";
  protected final String TEXT_67 = "();" + NL + "\t\tstatusListener_";
  protected final String TEXT_68 = ".addObserver(trCrawl_";
  protected final String TEXT_69 = ");" + NL + "\t\ttrCrawl_";
  protected final String TEXT_70 = ".addListener(statusListener_";
  protected final String TEXT_71 = ");" + NL + "\t\t" + NL + "\t\t//add Filter" + NL + "\t\ttrCrawl_";
  protected final String TEXT_72 = ".setExcludes(exclude_";
  protected final String TEXT_73 = ");" + NL + "\t\ttrCrawl_";
  protected final String TEXT_74 = ".setFroms(from_";
  protected final String TEXT_75 = ");" + NL + "\t\ttrCrawl_";
  protected final String TEXT_76 = ".setAts(at_";
  protected final String TEXT_77 = ");" + NL + "\t\ttrCrawl_";
  protected final String TEXT_78 = ".setFilter_links(";
  protected final String TEXT_79 = ");" + NL + "\t\t" + NL + "\t\ttrCrawl_";
  protected final String TEXT_80 = ".addFilter(fq_";
  protected final String TEXT_81 = ");" + NL + "\t\t" + NL + "\t// Link tweet details column to desired data";
  protected final String TEXT_82 = NL + " " + NL + " \twhile(nb_line_";
  protected final String TEXT_83 = " < ";
  protected final String TEXT_84 = "){" + NL + " \t\ttweets_";
  protected final String TEXT_85 = ".truncate();" + NL + "\t\ttrCrawl_";
  protected final String TEXT_86 = ".visit(tweets_";
  protected final String TEXT_87 = ");" + NL + "\t\t" + NL + "\t\tfor(";
  protected final String TEXT_88 = " singleTweet_";
  protected final String TEXT_89 = " : java.util.Arrays.asList(tweets_";
  protected final String TEXT_90 = ".getRows())){\t\t\t\t\t\t";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    

		CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
		INode node = (INode)codeGenArgument.getArgument();
		String cid = node.getUniqueName();
		 
		String TwitterLogger = "org.gabrielebaldassarre.twitter.commodities.logger.TwitterLogger";

		String TalendFlowController = "org.gabrielebaldassarre.tcomponent.bridge.TalendFlowController";
		String TalendFlowModel = "org.gabrielebaldassarre.tcomponent.bridge.TalendFlowModel";
		String TalendContext = "org.gabrielebaldassarre.tcomponent.bridge.TalendContext";
		String TalendFlowFactory = "org.gabrielebaldassarre.tcomponent.bridge.TalendFlowFactory";
		String TalendFlow = "org.gabrielebaldassarre.tcomponent.bridge.TalendFlow";
		String TalendType = "org.gabrielebaldassarre.tcomponent.bridge.TalendType";
		String TalendFlowQueryResultBehaviour = "org.gabrielebaldassarre.twitter.tweet.TalendFlowQueryResultBehaviour";
		String TalendFlowTweetBehaviour = "org.gabrielebaldassarre.twitter.tweet.TalendFlowTweetBehaviour";
		String TweetField = "org.gabrielebaldassarre.twitter.commodities.tweet.TweetField";
		String TalendRow = "org.gabrielebaldassarre.tcomponent.bridge.TalendRow";
		
		String TwitterQueryBuilderLogicalOperator = "org.gabrielebaldassarre.twitter.commodities.querybuilder.TwitterQueryBuilderLogicalOperator";  		

		String QueryResultField = "org.gabrielebaldassarre.twitter.commodities.tweet.QueryResultField";	
		String TalendRowTweetBehaviour = "org.gabrielebaldassarre.twitter.stream.tweet.TalendRowTweetBehaviour";
		
		String MyStatusListener = "org.gabrielebaldassarre.twitter.stream.tweet.listener.MyStatusListener";

    	/********************
    	 * DEFINES          *
     	 ********************/

     	List<IConnection> outMainConns = new ArrayList<IConnection>(); // outgoing main connections

     	List<IMetadataColumn> outMainColumns = null;

		// Parameters
		String logical = ElementParameterParser.getValue(node,"__LOGICAL_OP__");

		@SuppressWarnings("unchecked")    
		List<Map<String, String>> keywords = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node,  "__KEYWORDS__");
		
		@SuppressWarnings("unchecked")    
		List<Map<String, String>> languages = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node,  "__LANGUAGES__");
		
		@SuppressWarnings("unchecked")    
		List<Map<String, String>> conditions = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node,  "__CONDITIONS__");
		
		@SuppressWarnings("unchecked")
		List<Map<String, String>> mapping = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node,  "__MAPPING__");
		
		String oauth_connection = ElementParameterParser.getValue(node, "__OAUTH_CONNECTION__"); 

		// Advanced parameters
		String entitySeparator = ElementParameterParser.getValue(node,"__ENTITY_SEPARATOR__");
		boolean entityPrefix = ("true").equals(ElementParameterParser.getValue(node, "__ENTITY_PREFIX__"));
		

		Integer limit = Integer.parseInt(ElementParameterParser.getValue(node,"__LIMIT__").equals("") ? "1000" : ElementParameterParser.getValue(node,"__LIMIT__"));
		Integer rpp = 200;

		boolean filter_links = ("true").equals(ElementParameterParser.getValue(node, "__FILTER_LINKS__"));

		// Log parameters
		boolean send_to_logcatcher = ("true").equals(ElementParameterParser.getValue(node, "__SEND_TO_LOGCATCHER__"));
		
		// Decoding outgoing connections
    	for(IConnection conn: node.getOutgoingConnections("STANDARD")){
    		if(!conn.isActivate()) continue;
    		IConnection cc = NodeUtil.getRealConnectionTypeBased(conn);
    		outMainConns.add(cc);
    	}


    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(TwitterLogger);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(TwitterLogger);
    stringBuffer.append(TEXT_4);
     if(send_to_logcatcher == true) {
						if (node.getProcess().getNodesOfType("tLogCatcher").size() > 0) {
							@SuppressWarnings("unchecked")
							List<INode> logCatchers = (List<INode>)node.getProcess().getNodesOfType("tLogCatcher");
							for (INode logCatcher : logCatchers) {
								if (("true").equals(ElementParameterParser.getValue(logCatcher, "__CATCH_TWARN__"))) { 
    stringBuffer.append(TEXT_5);
    stringBuffer.append(logCatcher.getUniqueName() );
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_7);
    stringBuffer.append(logCatcher.getDesignSubjobStartNode().getUniqueName() );
    stringBuffer.append(TEXT_8);
    
								}
							}
						}
		 			} 
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    
		
		for(Map<String, String> data : keywords){
			stringBuffer.append("keywordList_"+cid+".add(" + data.get("KEYWORD") + ");");
 			stringBuffer.append(NL);
		}
		
		for(Map<String, String> data : languages){
			stringBuffer.append("languageList_"+cid+".add(\"" + data.get("LANGUAGE") + "\");");
 			stringBuffer.append(NL);
		}
		
		for(Map<String, String> data : conditions){
			String dataString = data.get("OPERATOR");
 			
 			if("EXCLUDE".equals(dataString)){
 				stringBuffer.append("excludeList_"+cid+".add(" + data.get("QUERY") + ");");
 				stringBuffer.append(NL);
 			}
 			
 			 if("AT".equals(dataString)){
 				stringBuffer.append("atList_"+cid+".add(" + data.get("QUERY") + ");");
 				stringBuffer.append(NL);
 			}
 			
 			 if("FROM".equals(dataString)){
 				stringBuffer.append("fromList_"+cid+".add(" + data.get("QUERY") + ");");
 				stringBuffer.append(NL);
 			}
		}
	
		
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(TalendFlowController);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(TalendFlowController);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(TalendFlowModel);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(TalendContext);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(TalendFlowFactory);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(TalendFlow);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(2*rpp);
    stringBuffer.append(TEXT_53);
    
if(outMainConns.size() > 0) stringBuffer.append(NL + "tweets_" + cid + NL); 
 if(outMainConns.size() > 0){
 
	IConnection conn = outMainConns.get(0);
	
	outMainColumns = conn.getMetadataTable().getListColumns();  
  	Map<String,IMetadataColumn> columnsMap = new HashMap<String,IMetadataColumn>();
  	for (IMetadataColumn col : outMainColumns) columnsMap.put(col.getLabel(),col);

	for(Map<String, String> data : mapping){
		String schemaColumn = data.get("SCHEMA_COLUMN"); 
		String type = columnsMap.get(schemaColumn).getTalendType();

		stringBuffer.append(".addColumn(\"" + schemaColumn +"\", " + TalendType + ".getInstanceFromTalendId(\"" + type + "\"))" + NL);
	}
}
if(outMainConns.size() > 0) stringBuffer.append(";" + NL);	

    stringBuffer.append(TEXT_54);
    stringBuffer.append(TalendRowTweetBehaviour);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(TalendRowTweetBehaviour);
    stringBuffer.append(TEXT_57);
    stringBuffer.append( oauth_connection );
    stringBuffer.append(TEXT_58);
    stringBuffer.append(entitySeparator);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(entityPrefix);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(TwitterQueryBuilderLogicalOperator);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(logical);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(MyStatusListener);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(MyStatusListener);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(filter_links);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_81);
    
	if(outMainConns.size() > 0) stringBuffer.append(NL + "trCrawl_" + cid + NL);
	if(outMainConns.size() > 0){

	for(Map<String, String> data : mapping){
		String schemaColumn = data.get("SCHEMA_COLUMN"); 
		String operation = data.get("OPERATION"); 

		stringBuffer.append(".setColumnLink(tweets_" + cid + ".getColumn(\"" + schemaColumn + "\"), " + TweetField + "." + operation + ")" + NL);
	}
}
if(outMainConns.size() > 0) stringBuffer.append(";" + NL);
 
    stringBuffer.append(TEXT_82);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(limit);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(TalendRow);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_90);
    return stringBuffer.toString();
  }
}
