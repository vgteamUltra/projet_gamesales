package org.talend.designer.codegen.translators.socialanalytics.twitter;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.utils.NodeUtil;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class TTwitterStreamInputMainJava
{
  protected static String nl;
  public static synchronized TTwitterStreamInputMainJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TTwitterStreamInputMainJava result = new TTwitterStreamInputMainJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "nb_line_";
  protected final String TEXT_2 = "++;";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

		/********************
    	 * DEFINES          *
     	 ********************/

     	List<IConnection> outMainConns = new ArrayList<IConnection>(); // outgoing main connections

     	List<IMetadataColumn> outMainColumns = null;
     	
     	// Parameters
     	@SuppressWarnings("unchecked")
		List<Map<String, String>> mapping = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node,  "__MAPPING__");
		
     	
     	// Decoding outgoing connections
    	for(IConnection conn: node.getOutgoingConnections("STANDARD")){
    		if(!conn.isActivate()) continue;
    		IConnection cc = NodeUtil.getRealConnectionTypeBased(conn);
    		outMainConns.add(cc);
    	}

    
for (IConnection conn : outMainConns){ //2
  
  	outMainColumns = conn.getMetadataTable().getListColumns();  
  	Map<String,IMetadataColumn> columnsMap = new HashMap<String,IMetadataColumn>();
  	for (IMetadataColumn col : outMainColumns) columnsMap.put(col.getLabel(),col);
  
    stringBuffer.append(NL);
    stringBuffer.append("/**" + NL);
    stringBuffer.append(" * start of outgoing connection: " + conn.getName() + NL);
    stringBuffer.append(" */" + NL);
    stringBuffer.append(conn.getName() + " = new " + conn.getName() + "Struct();");

	for(Map<String, String> data : mapping){
		stringBuffer.append(NL);
		stringBuffer.append("/* column: " + data.get("SCHEMA_COLUMN") + " */ ");
		stringBuffer.append(NL);
		//String typeToCast = JavaTypesManager.getTypeToGenerate(columnsMap.get(data.get("SCHEMA_COLUMN")).getTalendType(),columnsMap.get(data.get("SCHEMA_COLUMN")).isNullable());
		String typeToCast = JavaTypesManager.getTypeToGenerate(columnsMap.get(data.get("SCHEMA_COLUMN")).getTalendType(),true);
		
		String toPrimitive = JavaTypesManager.isJavaPrimitiveType(columnsMap.get(data.get("SCHEMA_COLUMN")).getTalendType(),columnsMap.get(data.get("SCHEMA_COLUMN")).isNullable()) ? "." + JavaTypesManager.getTypeToGenerate(columnsMap.get(data.get("SCHEMA_COLUMN")).getTalendType(),false) + "Value()" : "";
		stringBuffer.append(conn.getName() + "." + data.get("SCHEMA_COLUMN") + " = ((" + typeToCast + ") singleTweet_" + cid + ".getValue(\"" + data.get("SCHEMA_COLUMN") + "\"))" + toPrimitive + ";");
		stringBuffer.append(NL);

	}
	
	 stringBuffer.append("/**" + NL);
    stringBuffer.append(" * end of outgoing connection: " + conn.getName() + NL);
    stringBuffer.append(" */" + NL);
    
  }

 
    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    return stringBuffer.toString();
  }
}
