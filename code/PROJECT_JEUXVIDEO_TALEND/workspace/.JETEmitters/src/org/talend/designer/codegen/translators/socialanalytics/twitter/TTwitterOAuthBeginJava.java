package org.talend.designer.codegen.translators.socialanalytics.twitter;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.designer.codegen.config.CodeGeneratorArgument;

public class TTwitterOAuthBeginJava
{
  protected static String nl;
  public static synchronized TTwitterOAuthBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TTwitterOAuthBeginJava result = new TTwitterOAuthBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "twitter4j.conf.ConfigurationBuilder cb_";
  protected final String TEXT_2 = " = new twitter4j.conf.ConfigurationBuilder();" + NL + "cb_";
  protected final String TEXT_3 = ".setOAuthConsumerKey(";
  protected final String TEXT_4 = ");" + NL + "cb_";
  protected final String TEXT_5 = ".setOAuthConsumerSecret(";
  protected final String TEXT_6 = ");" + NL + "cb_";
  protected final String TEXT_7 = ".setOAuthAccessToken(";
  protected final String TEXT_8 = ");" + NL + "cb_";
  protected final String TEXT_9 = ".setOAuthAccessTokenSecret(";
  protected final String TEXT_10 = ");" + NL + "" + NL + "" + NL + "/**" + NL + " * TODO: it's better to enable json ONLY if there are some json connection in the whole job" + NL + " */" + NL + "cb_";
  protected final String TEXT_11 = ".setJSONStoreEnabled(true);" + NL + "" + NL + "" + NL + "twitter4j.";
  protected final String TEXT_12 = "Factory twitter_factory_";
  protected final String TEXT_13 = " = new twitter4j.";
  protected final String TEXT_14 = "Factory(cb_";
  protected final String TEXT_15 = ".build());" + NL + "twitter4j.";
  protected final String TEXT_16 = " twitter_";
  protected final String TEXT_17 = " = twitter_factory_";
  protected final String TEXT_18 = ".getInstance();" + NL + "globalMap.put(\"twitter_";
  protected final String TEXT_19 = "\",twitter_";
  protected final String TEXT_20 = ");";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

// Parameters
String consumer_key = ElementParameterParser.getValue(node,"__CONSUMER_KEY__");
String consumer_secret = ElementParameterParser.getValue(node,"__CONSUMER_SECRET__");
String access_token = ElementParameterParser.getValue(node,"__ACCESS_TOKEN__");
String access_token_secret = ElementParameterParser.getValue(node,"__ACCESS_TOKEN_SECRET__");
String connection_type = ElementParameterParser.getValue(node,"__CONNECTION_TYPE__");


    stringBuffer.append(TEXT_1);
    stringBuffer.append( cid );
    stringBuffer.append(TEXT_2);
    stringBuffer.append( cid );
    stringBuffer.append(TEXT_3);
    stringBuffer.append( consumer_key );
    stringBuffer.append(TEXT_4);
    stringBuffer.append( cid );
    stringBuffer.append(TEXT_5);
    stringBuffer.append( consumer_secret );
    stringBuffer.append(TEXT_6);
    stringBuffer.append( cid );
    stringBuffer.append(TEXT_7);
    stringBuffer.append( access_token );
    stringBuffer.append(TEXT_8);
    stringBuffer.append( cid );
    stringBuffer.append(TEXT_9);
    stringBuffer.append( access_token_secret );
    stringBuffer.append(TEXT_10);
    stringBuffer.append( cid );
    stringBuffer.append(TEXT_11);
    stringBuffer.append( connection_type );
    stringBuffer.append(TEXT_12);
    stringBuffer.append( cid );
    stringBuffer.append(TEXT_13);
    stringBuffer.append( connection_type );
    stringBuffer.append(TEXT_14);
    stringBuffer.append( cid );
    stringBuffer.append(TEXT_15);
    stringBuffer.append( connection_type );
    stringBuffer.append(TEXT_16);
    stringBuffer.append( cid );
    stringBuffer.append(TEXT_17);
    stringBuffer.append( cid );
    stringBuffer.append(TEXT_18);
    stringBuffer.append( cid );
    stringBuffer.append(TEXT_19);
    stringBuffer.append( cid );
    stringBuffer.append(TEXT_20);
    return stringBuffer.toString();
  }
}
