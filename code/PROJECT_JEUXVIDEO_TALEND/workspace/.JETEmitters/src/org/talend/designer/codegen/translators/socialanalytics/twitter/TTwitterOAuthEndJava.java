package org.talend.designer.codegen.translators.socialanalytics.twitter;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.designer.codegen.config.CodeGeneratorArgument;

public class TTwitterOAuthEndJava
{
  protected static String nl;
  public static synchronized TTwitterOAuthEndJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TTwitterOAuthEndJava result = new TTwitterOAuthEndJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "globalMap.put(\"";
  protected final String TEXT_2 = "_USER_ID\", twitter_";
  protected final String TEXT_3 = ".getId());" + NL + "globalMap.put(\"";
  protected final String TEXT_4 = "_SCREEN_NAME\", twitter_";
  protected final String TEXT_5 = ".getScreenName());";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_2);
    stringBuffer.append( cid );
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_4);
    stringBuffer.append( cid );
    stringBuffer.append(TEXT_5);
    return stringBuffer.toString();
  }
}
