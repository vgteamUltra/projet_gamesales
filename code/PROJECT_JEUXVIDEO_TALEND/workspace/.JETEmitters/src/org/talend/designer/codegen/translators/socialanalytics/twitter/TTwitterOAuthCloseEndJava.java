package org.talend.designer.codegen.translators.socialanalytics.twitter;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.designer.codegen.config.CodeGeneratorArgument;

public class TTwitterOAuthCloseEndJava
{
  protected static String nl;
  public static synchronized TTwitterOAuthCloseEndJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TTwitterOAuthCloseEndJava result = new TTwitterOAuthCloseEndJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "if(((twitter4j.TwitterBase)globalMap.get(\"twitter_";
  protected final String TEXT_2 = "\")).getClass().equals(twitter4j.TwitterStream.class)) {" + NL + "\t((twitter4j.TwitterStream)globalMap.get(\"twitter_";
  protected final String TEXT_3 = "\")).shutdown();" + NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

String oauth_connection = ElementParameterParser.getValue(node, "__OAUTH_CONNECTION__"); 

    stringBuffer.append(TEXT_1);
    stringBuffer.append( oauth_connection );
    stringBuffer.append(TEXT_2);
    stringBuffer.append( oauth_connection );
    stringBuffer.append(TEXT_3);
    return stringBuffer.toString();
  }
}
