package org.talend.designer.codegen.translators.socialanalytics.twitter;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.utils.NodeUtil;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.HashMap;

public class TTwitterInputBeginJava
{
  protected static String nl;
  public static synchronized TTwitterInputBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TTwitterInputBeginJava result = new TTwitterInputBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t\t/**" + NL + "\t\t * Talend Bridge Logger" + NL + "\t\t * @see http://gabrielebaldassarre.com" + NL + "\t\t */" + NL + "\t\t" + NL + "\t\tclass TalendTwitterLogger_";
  protected final String TEXT_2 = " implements java.util.Observer{" + NL + "" + NL + "\t\t\t   public void update(java.util.Observable obs, Object obj)" + NL + "\t\t\t   {" + NL + "\t\t\t   ";
  protected final String TEXT_3 = " log = (";
  protected final String TEXT_4 = ")obj;" + NL + "\t\t\t   ";
  protected final String TEXT_5 = NL + "\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_6 = ".addMessage(\"tWarn\", \"";
  protected final String TEXT_7 = "\", 4, log.getMessage(), 210);" + NL + "\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_8 = "Process(globalMap);" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_9 = NL + "\t\t\t   \tresumeUtil.addLog(log.getCategory(), \"NODE:";
  protected final String TEXT_10 = "\", \"\", log.getThreadId() + \"\", log.getSeverity(),\"\", log.getMessage(),\"\", \"\");" + NL + "\t\t\t\t   System.err.println(log.getMessage());" + NL + "\t\t\t   }" + NL + "\t\t\t" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\tTalendTwitterLogger_";
  protected final String TEXT_11 = " twitterLogger_";
  protected final String TEXT_12 = " = new TalendTwitterLogger_";
  protected final String TEXT_13 = "();" + NL + "" + NL + "int nb_line_";
  protected final String TEXT_14 = " = 0;" + NL + " ";
  protected final String TEXT_15 = NL;
  protected final String TEXT_16 = " s_";
  protected final String TEXT_17 = " = new ";
  protected final String TEXT_18 = "();" + NL + "s_";
  protected final String TEXT_19 = NL + "\t.logicalOperator(";
  protected final String TEXT_20 = ".";
  protected final String TEXT_21 = ")" + NL + "\t";
  protected final String TEXT_22 = NL + "\t.attitude(";
  protected final String TEXT_23 = ".";
  protected final String TEXT_24 = ")" + NL + "\t.filterLinks(";
  protected final String TEXT_25 = ")" + NL + "\t.filterQuestions(";
  protected final String TEXT_26 = ")";
  protected final String TEXT_27 = NL + "\t.advancedQuery(";
  protected final String TEXT_28 = ")";
  protected final String TEXT_29 = NL + "\t";
  protected final String TEXT_30 = ".resultType(";
  protected final String TEXT_31 = ".";
  protected final String TEXT_32 = ") ";
  protected final String TEXT_33 = NL + "\t";
  protected final String TEXT_34 = ".lang(";
  protected final String TEXT_35 = ")";
  protected final String TEXT_36 = NL + "\t";
  protected final String TEXT_37 = ".maxId(";
  protected final String TEXT_38 = ")";
  protected final String TEXT_39 = NL + "\t";
  protected final String TEXT_40 = ".since(";
  protected final String TEXT_41 = ")";
  protected final String TEXT_42 = NL + "\t";
  protected final String TEXT_43 = ".sinceId(";
  protected final String TEXT_44 = ")";
  protected final String TEXT_45 = NL + "\t";
  protected final String TEXT_46 = ".until(";
  protected final String TEXT_47 = ")";
  protected final String TEXT_48 = NL + "\t\t;" + NL + "" + NL + "\t\t/**" + NL + "\t\t * TOS-bridge data structures" + NL + "\t\t * @see http://gabrielebaldassarre.com" + NL + "\t\t */" + NL + "\t\t " + NL + "\t\t// data model initialization      " + NL + "\t\t";
  protected final String TEXT_49 = " tcontroller_";
  protected final String TEXT_50 = " = ";
  protected final String TEXT_51 = ".getInstance();" + NL + "\t\t";
  protected final String TEXT_52 = " tmodel_";
  protected final String TEXT_53 = " = tcontroller_";
  protected final String TEXT_54 = ".getModel(new ";
  protected final String TEXT_55 = "(globalMap));" + NL + "" + NL + "\t\t// flows factory interface" + NL + "\t\t";
  protected final String TEXT_56 = " tablefactory_";
  protected final String TEXT_57 = " = tmodel_";
  protected final String TEXT_58 = ".getFlowFactory();" + NL + "\t\t        " + NL + "\t\t// init needed data flows" + NL + "\t\t";
  protected final String TEXT_59 = " queryResults_";
  protected final String TEXT_60 = " = tablefactory_";
  protected final String TEXT_61 = ".newFlow(\"Twitter Query Results\", 0, false);" + NL + "\t\t";
  protected final String TEXT_62 = " tweets_";
  protected final String TEXT_63 = " = tablefactory_";
  protected final String TEXT_64 = ".newFlow(\"Tweets\", ";
  protected final String TEXT_65 = ", false);" + NL + "      \t\t" + NL + "\t\t// Prepare columns for tweets details data flow";
  protected final String TEXT_66 = NL + "\t\t" + NL + "\t\t// Prepare visitors" + NL + "\t\t";
  protected final String TEXT_67 = " qrCrawl_";
  protected final String TEXT_68 = " = new ";
  protected final String TEXT_69 = "((twitter4j.TwitterBase)globalMap.get(\"twitter_";
  protected final String TEXT_70 = "\"), s_";
  protected final String TEXT_71 = ".build(), ";
  protected final String TEXT_72 = ");" + NL + "\t\t";
  protected final String TEXT_73 = " twCrawl_";
  protected final String TEXT_74 = " = new ";
  protected final String TEXT_75 = "(";
  protected final String TEXT_76 = ", ";
  protected final String TEXT_77 = ");" + NL + "\t\t" + NL + "\t\tqrCrawl_";
  protected final String TEXT_78 = ".addObserver(twitterLogger_";
  protected final String TEXT_79 = ");" + NL + "\t\ttwCrawl_";
  protected final String TEXT_80 = ".addObserver(twitterLogger_";
  protected final String TEXT_81 = ");" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t// Link tweet details column to desired data";
  protected final String TEXT_82 = NL + "\t\tqrCrawl_";
  protected final String TEXT_83 = ".visit(queryResults_";
  protected final String TEXT_84 = ");" + NL + "\t\t" + NL + "\t\tfor(";
  protected final String TEXT_85 = " page_";
  protected final String TEXT_86 = " : qrCrawl_";
  protected final String TEXT_87 = "){" + NL + "\t\t\ttweets_";
  protected final String TEXT_88 = ".truncate();" + NL + "\t\t\ttwCrawl_";
  protected final String TEXT_89 = ".setInput(page_";
  protected final String TEXT_90 = ".getTalendValue(\"statusSet\"));" + NL + "\t\t\ttwCrawl_";
  protected final String TEXT_91 = ".visit(tweets_";
  protected final String TEXT_92 = ");" + NL + "" + NL + "\t\t\tfor(";
  protected final String TEXT_93 = " singleTweet_";
  protected final String TEXT_94 = " : java.util.Arrays.asList(tweets_";
  protected final String TEXT_95 = ".getRows())){\t\t\t\t\t\t";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
		CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
		INode node = (INode)codeGenArgument.getArgument();
		String cid = node.getUniqueName();


		/********************
		 * IMPORTS          *
		 ********************/
		 
		String TwitterLogger = "org.gabrielebaldassarre.twitter.commodities.logger.TwitterLogger";
		String TwitterQueryBuilder = "org.gabrielebaldassarre.twitter.commodities.querybuilder.TwitterQueryBuilder";
		String TwitterQueryBuilderOperator = "org.gabrielebaldassarre.twitter.commodities.querybuilder.TwitterQueryBuilderOperator";  
		String TwitterQueryBuilderLogicalOperator = "org.gabrielebaldassarre.twitter.commodities.querybuilder.TwitterQueryBuilderLogicalOperator";  
		String TwitterQueryBuilderAttitude = "org.gabrielebaldassarre.twitter.commodities.querybuilder.TwitterQueryBuilderAttitude";
		String TwitterQueryBuilderResultTypes = "org.gabrielebaldassarre.twitter.commodities.querybuilder.TwitterQueryBuilderResultTypes";
		String TalendFlowController = "org.gabrielebaldassarre.tcomponent.bridge.TalendFlowController";
		String TalendFlowModel = "org.gabrielebaldassarre.tcomponent.bridge.TalendFlowModel";
		String TalendContext = "org.gabrielebaldassarre.tcomponent.bridge.TalendContext";
		String TalendFlowFactory = "org.gabrielebaldassarre.tcomponent.bridge.TalendFlowFactory";
		String TalendFlow = "org.gabrielebaldassarre.tcomponent.bridge.TalendFlow";
		String TalendType = "org.gabrielebaldassarre.tcomponent.bridge.TalendType";
		String TalendFlowQueryResultBehaviour = "org.gabrielebaldassarre.twitter.tweet.TalendFlowQueryResultBehaviour";
		String TalendFlowTweetBehaviour = "org.gabrielebaldassarre.twitter.tweet.TalendFlowTweetBehaviour";
		String TweetField = "org.gabrielebaldassarre.twitter.commodities.tweet.TweetField";
		String TalendRow = "org.gabrielebaldassarre.tcomponent.bridge.TalendRow";

    	/********************
    	 * DEFINES          *
     	 ********************/

     	List<IConnection> outMainConns = new ArrayList<IConnection>(); // outgoing main connections
     	List<IConnection> outJsonConns = new ArrayList<IConnection>(); // outgoing json connections

     	List<IMetadataColumn> outMainColumns = null;

		// Parameters
		String logical = ElementParameterParser.getValue(node,"__LOGICAL_OP__");
		boolean use_advanced = ("true").equals(ElementParameterParser.getValue(node, "__USE_ADVANCED__"));
		String advanced_condition = ElementParameterParser.getValue(node, "__ADVANCED_COND__");
		
		@SuppressWarnings("unchecked")    
		List<Map<String, String>> conditions = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node,  "__CONDITIONS__");
		
		@SuppressWarnings("unchecked")
		List<Map<String, String>> mapping = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node,  "__MAPPING__");
		
		String oauth_connection = ElementParameterParser.getValue(node, "__OAUTH_CONNECTION__"); 

		// Advanced parameters
		String attitude = ElementParameterParser.getValue(node,"__FILTER_ATTITUDE__");
		String since = ElementParameterParser.getValue(node,"__SINCE__");
		String until = ElementParameterParser.getValue(node,"__UNTIL__");
		String since_id = ElementParameterParser.getValue(node,"__SINCE_ID__");
		String max_id = ElementParameterParser.getValue(node,"__MAX_ID__");
		String language = ElementParameterParser.getValue(node,"__LANGUAGE__");
		String result_type = ElementParameterParser.getValue(node,"__RESULT_TYPE__");
		String entitySeparator = ElementParameterParser.getValue(node,"__ENTITY_SEPARATOR__");
		boolean entityPrefix = ("true").equals(ElementParameterParser.getValue(node, "__ENTITY_PREFIX__"));
		

		Integer limit = Integer.parseInt(ElementParameterParser.getValue(node,"__LIMIT__").equals("") ? "1000" : ElementParameterParser.getValue(node,"__LIMIT__"));
		Integer rpp = 200;

		boolean filter_links = ("true").equals(ElementParameterParser.getValue(node, "__FILTER_LINKS__"));
		boolean filter_questions = ("true").equals(ElementParameterParser.getValue(node, "__FILTER_QUESTIONS__"));

		// Log parameters
		boolean send_to_logcatcher = ("true").equals(ElementParameterParser.getValue(node, "__SEND_TO_LOGCATCHER__"));

		boolean hasJsonConnections = (node.getOutgoingConnections("JSON").size() != 0);
		
		// Decoding outgoing connections
		for(IConnection conn: node.getOutgoingConnections("JSON")){
    		if(!conn.isActivate()) continue;
    		IConnection cc = NodeUtil.getRealConnectionTypeBased(conn);
    		outJsonConns.add(cc);
    	}
		
    	for(IConnection conn: node.getOutgoingConnections("STANDARD")){
    		if(!conn.isActivate()) continue;
    		IConnection cc = NodeUtil.getRealConnectionTypeBased(conn);
    		outMainConns.add(cc);
    	}


    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(TwitterLogger);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(TwitterLogger);
    stringBuffer.append(TEXT_4);
     if(send_to_logcatcher == true) {
						if (node.getProcess().getNodesOfType("tLogCatcher").size() > 0) {
							@SuppressWarnings("unchecked")
							List<INode> logCatchers = (List<INode>)node.getProcess().getNodesOfType("tLogCatcher");
							for (INode logCatcher : logCatchers) {
								if (("true").equals(ElementParameterParser.getValue(logCatcher, "__CATCH_TWARN__"))) { 
    stringBuffer.append(TEXT_5);
    stringBuffer.append(logCatcher.getUniqueName() );
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_7);
    stringBuffer.append(logCatcher.getDesignSubjobStartNode().getUniqueName() );
    stringBuffer.append(TEXT_8);
    
								}
							}
						}
		 			} 
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(TwitterQueryBuilder);
    stringBuffer.append(TEXT_16);
    stringBuffer.append( cid );
    stringBuffer.append(TEXT_17);
    stringBuffer.append(TwitterQueryBuilder);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
     if(use_advanced == false){ 
    stringBuffer.append(TEXT_19);
    stringBuffer.append(TwitterQueryBuilderLogicalOperator);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(logical);
    stringBuffer.append(TEXT_21);
    
	for(Map<String, String> data : conditions){
		stringBuffer.append(".condition(" + data.get("QUERY") + ", " + TwitterQueryBuilderOperator + "." + data.get("OPERATOR") + ")");
 		stringBuffer.append(NL);
	}

    stringBuffer.append(TEXT_22);
    stringBuffer.append(TwitterQueryBuilderAttitude);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(attitude);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(filter_links);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(filter_questions);
    stringBuffer.append(TEXT_26);
     } else { 
    stringBuffer.append(TEXT_27);
    stringBuffer.append(advanced_condition);
    stringBuffer.append(TEXT_28);
     } 
    stringBuffer.append(TEXT_29);
     if(!"MIXED".equals(result_type)){ 
    stringBuffer.append(TEXT_30);
    stringBuffer.append(TwitterQueryBuilderResultTypes);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(result_type);
    stringBuffer.append(TEXT_32);
     } 
    stringBuffer.append(TEXT_33);
     if(language.length() > 0) { 
    stringBuffer.append(TEXT_34);
    stringBuffer.append(language);
    stringBuffer.append(TEXT_35);
     } 
    stringBuffer.append(TEXT_36);
     if(max_id.length() > 0) { 
    stringBuffer.append(TEXT_37);
    stringBuffer.append(max_id);
    stringBuffer.append(TEXT_38);
     } 
    stringBuffer.append(TEXT_39);
     if(since.length() > 0) { 
    stringBuffer.append(TEXT_40);
    stringBuffer.append(since);
    stringBuffer.append(TEXT_41);
     } 
    stringBuffer.append(TEXT_42);
     if(since_id.length() > 0) { 
    stringBuffer.append(TEXT_43);
    stringBuffer.append(since_id);
    stringBuffer.append(TEXT_44);
     } 
    stringBuffer.append(TEXT_45);
     if(until.length() > 0) { 
    stringBuffer.append(TEXT_46);
    stringBuffer.append(until);
    stringBuffer.append(TEXT_47);
     } 
    stringBuffer.append(TEXT_48);
    stringBuffer.append(TalendFlowController);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(TalendFlowController);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(TalendFlowModel);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(TalendContext);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(TalendFlowFactory);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(TalendFlow);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(TalendFlow);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(2*rpp);
    stringBuffer.append(TEXT_65);
    
if(outMainConns.size() > 0 || hasJsonConnections == true) stringBuffer.append(NL + "tweets_" + cid + NL); 
 if(outMainConns.size() > 0){
 
	IConnection conn = outMainConns.get(0);
	
	outMainColumns = conn.getMetadataTable().getListColumns();  
  	Map<String,IMetadataColumn> columnsMap = new HashMap<String,IMetadataColumn>();
  	for (IMetadataColumn col : outMainColumns) columnsMap.put(col.getLabel(),col);

	for(Map<String, String> data : mapping){
		String schemaColumn = data.get("SCHEMA_COLUMN"); 
		String type = columnsMap.get(schemaColumn).getTalendType();

		stringBuffer.append(".addColumn(\"" + schemaColumn +"\", " + TalendType + ".getInstanceFromTalendId(\"" + type + "\"))" + NL);
	}
}
if(hasJsonConnections == true) stringBuffer.append(".addColumn(\"jsonString\", " + TalendType + ".getInstanceFromTalendId(\"id_String\"))" + NL);
if(outMainConns.size() > 0 || hasJsonConnections == true) stringBuffer.append(";" + NL);	

    stringBuffer.append(TEXT_66);
    stringBuffer.append(TalendFlowQueryResultBehaviour);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(TalendFlowQueryResultBehaviour);
    stringBuffer.append(TEXT_69);
    stringBuffer.append( oauth_connection );
    stringBuffer.append(TEXT_70);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(limit);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(TalendFlowTweetBehaviour);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(TalendFlowTweetBehaviour);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(entitySeparator);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(entityPrefix);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_81);
    
	if(outMainConns.size() > 0 || hasJsonConnections == true) stringBuffer.append(NL + "twCrawl_" + cid + NL);
	if(outMainConns.size() > 0){

	for(Map<String, String> data : mapping){
		String schemaColumn = data.get("SCHEMA_COLUMN"); 
		String operation = data.get("OPERATION"); 

		stringBuffer.append(".setColumnLink(tweets_" + cid + ".getColumn(\"" + schemaColumn + "\"), " + TweetField + "." + operation + ")" + NL);
	}
}
if(hasJsonConnections == true) stringBuffer.append(".setColumnLink(tweets_" + cid + ".getColumn(\"jsonString\"), " + TweetField + ".JSON)" + NL);
if(outMainConns.size() > 0 || hasJsonConnections == true) stringBuffer.append(";" + NL);
 
    stringBuffer.append(TEXT_82);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(TalendRow);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_90);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_91);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_92);
    stringBuffer.append(TalendRow);
    stringBuffer.append(TEXT_93);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_94);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_95);
    return stringBuffer.toString();
  }
}
