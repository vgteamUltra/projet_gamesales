import random
import operator
from timeit import Timer
 
ITERATIONS = 100
 
def f1(liste):
    liste.sort(key=operator.itemgetter(2))
    return liste
 
def f2(liste):
    l = [(j[2], i) for i,j in enumerate(liste)]
    l.sort()
    r = [liste[item[1]] for item in l]
    return r
 
 
def f3(liste):
    liste.sort()
    return liste
 
if __name__ == '__main__':
    liste = []
    for i in range(5000):
        item = (1, 2,
		random.randint(0, 5000))
        liste.append(item)
 
    t1 = Timer('f1(liste)', 'from __main__ import f1, liste')
    t2 = Timer('f2(liste)', 'from __main__ import f2, liste')
    t3 = Timer('f3(liste)', 'from __main__ import f3, liste')
    print "\tf1:", min(t1.repeat(3,ITERATIONS)), "s"
    print "\tf2:", min(t2.repeat(3,ITERATIONS)), "s"
    print "\tf3:", min(t3.repeat(3,ITERATIONS)), "s"
