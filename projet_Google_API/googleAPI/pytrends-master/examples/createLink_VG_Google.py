import time
import itertools
import difflib
sm = difflib.SequenceMatcher(None)
from operator import itemgetter

import pymongo                          
from pymongo import MongoClient
client = MongoClient('84.39.39.108')
db = client.test_db

def suppSpace(exp):
    return exp.strip()

def print_liste(exp):
    print exp

def printType_liste(exp):
    print type(exp)
    
def convert_liste(exp):
    return str(exp)

col0=db.ref_vgchart_weekly_list_publisher
liste0=col0.find_one()['Publisher']
#liste0=map(suppSpace, liste0)
#liste0.remove(' Misc')
liste0=map(convert_liste, liste0)
print liste0

col1=db.googleTrend_publisher
liste1=[]
for doc in col1.find():
    liste1.append(str(doc["_id"]))
print liste1

#map(printType_liste,liste0)
#print '---'
#map(printType_liste,liste1)

# !!!!! == supress Sony Computer Entertainment Europe and affecte sony to Sony Computer Entertainment Europe
# !!!!! == supress Milestone S.r.l and affecte milestone to Milestone S.r.l
# !!!!! == pb Square Enix vs Square Enix[space] ...

listeMatch_indice=[[0,1],[1,0],[2,14],[3,0],[3,1],[4,0],[5,0],[6,0],[7,0],\
                  [9,0],[10,0],[11,4],[12,0],[12,1],[13,0],[14,0],[15,9],\
                  [17,0],[18,5],[20,0],[21,2],[22,0],[23,0],[24,0],[25,2],\
                  [26,2],[27,0],[28,0],[29,0],[30,0],[31,0],[32,0],[33,0],[33,1],\
                  [34,0],[35,0],[36,0],[37,0],[38,0]]
#add [['Sony Computer Entertainment','sony'],['Sony Computer Entertainment Europe','sony']]
#add [['Warner Bros. Interactive Entertainment','warner']]
#add [['THQ','thq']]

#f=open("test_output/createLink_output.txt","w")
match_=[]
goodMatch=[]
match_indice=[]
for i, p1 in enumerate(liste1):
    match_=[]
    sm.set_seq1(p1)
    for p0 in liste0:
        sm.set_seq2(p0)
        match_.append((sm.ratio(), p1, p0))
    match_.sort(key=itemgetter(0), reverse = True)
    #print "---"
    #map(print_liste,match_[:10])
    #f.write("===\n")
    for j, corr in enumerate(match_[:20]):
         match_indice=[i,j]
         if match_indice in listeMatch_indice:
             #print type(corr[2]),', ',type(corr[1])
             goodMatch.append([corr[2],corr[1]])
    #    f.write("---\n")
    #    f.write(str(i))
    #    f.write(", ")
    #    f.write(str(j))
    #    f.write(": ")
    #    f.write(str(corr[0]))
    #    f.write(", ")
    #    f.write(corr[1])
    #    f.write(", ")
    #    f.write(corr[2])
    #    f.write("\n")
goodMatch.append(['Sony Computer Entertainment','sony'])
goodMatch.append(['Sony Computer Entertainment Europe','sony'])
goodMatch.append(['Warner Bros. Interactive Entertainment','warner'])
goodMatch.append(['THQ','thq'])

print len(goodMatch)
#for elem in goodMatch:
#    print elem[0],', ',elem[1]

#f.close()

'''dict_corr={}
for p0, p1, in itertools.izip(liste0, liste1):
    #dict_corr[p0]=p1
    print p0,', ',p1
print dict_corr  '''  
    

#col2=db.vgchart_weekly
for i, item in enumerate(goodMatch):
     #if i<10: print type(item)
     #print item,", ",dict_corr[item]
     print item[0],', ',item[1]
     #col2.update_many({"Publisher":item[0]},{'$set' : {"Google_Publisher_Name_Format":item[1]}})
     print i+1
     
print "Finish!"
