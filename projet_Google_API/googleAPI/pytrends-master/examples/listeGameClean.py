from pytrends.request import TrendReq
import json
import pymongo                          
from pymongo import MongoClient

client = MongoClient('84.39.39.108')
db = client.test_db

#col = db.ref_liste_gameClean
col=db.ref_vgchart_weekly_list_game

#cleaning names of games
listeGame = col.find_one()['Game']
gameClean = ""
listeGameClean=[]
dict_={}

def unifyList(seq): # Order preserving
  #Modified version of Dave Kirby solution
  seen = set()
  return [x for x in seq if x not in seen and not seen.add(x)]

def minus(phrase):
	outphrase=phrase.lower()
	return outphrase

def noSymbol(phrase):
	mots=phrase.split()
	outphrase=""
	for mot in mots:
		if mot[-1]==':': outphrase += mot[:-1] + " "
		else: outphrase += mot + " "
	return outphrase[:-1]
		

for game in listeGame:
	game = str(game)
	game = game.replace("&"," ")
	game = game.replace("/"," ")
	game = game.replace(":"," ")
	game = game.replace("-"," ")
	game = game.replace("+"," ")
	game = game.replace("'"," ")
	gameSplit = game.split()
	gameClean = ""
	for g in gameSplit : 
		if (g=="&" or g=="/" or g=="-"): continue
		gameClean += g + " "
		gameCleanSplit = gameClean.split()
	if len(gameCleanSplit) > 3:
		gameCleanSplit=gameCleanSplit[:3]
	gameClean = ""
	for g in gameCleanSplit:
		gameClean += g + " "	
	print gameClean, "\n"
	listeGameClean.append(gameClean)

listeGameClean=map(minus, listeGameClean)
listeGameClean=map(noSymbol, listeGameClean)
listeGameClean=unifyList(listeGameClean)

dict_["Game"]=listeGameClean
colout = db.ref_list_gameCleanLowerNoSymb_unique
post_id = colout.insert_one(dict_).inserted_id
