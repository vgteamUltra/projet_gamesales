# coding: utf-8

import time
import datetime
from datetime import datetime
import itertools
from operator import itemgetter

import difflib
sm = difflib.SequenceMatcher(None)

#sm.set_seq2('Batman: Arkham City')
#SequenceMatcher computes and caches detailed information
#about the second sequence, so if you want to compare one
#sequence against many sequences, use set_seq2() to set
#the commonly used sequence once and call set_seq1()
#repeatedly, once for each of the other sequences.
# (the doc)
'''
for x in ('batman: arkham city',
          'batman arkham city',
          'the batman: arkham city',
          'batman',
          'The / Batman: Arkham-City!',
	   'Football Manager 2012','Football', 'batman arkham origins','last of','lego batman','lego: batman','lego:batman'):
    sm.set_seq1(x)
    print x,sm.ratio()
'''

def unifyList(seq): # Order preserving
  #Modified version of Dave Kirby solution
  seen = set()
  return [x for x in seq if x not in seen and not seen.add(x)]

def isInt_(string_):
	try:
        	isinstance(int(string_), int)
		return True
    	except ValueError:
		return False        

def truncateExp(exp):
	expout=""
	expSplit=exp.split()
	for mot in expSplit[:-1]:
		 expout += mot+" "
	return expout[:-1]

import pymongo                          
from pymongo import MongoClient
client = MongoClient('84.39.39.108')
db = client.test_db

#========================================
col1=db.googleTrend
liste1=[]
for doc in col1.find():
	liste1.append(str(doc['_id']))
liste1.sort()
#for game in liste1: print game

#print str(datetime.now())
time1=time.time()
col2=db.vgchart_weekly
liste2=[]
for doc in col2.find():
	liste2.append(str(doc['Game']))
liste2=unifyList(liste2)
liste2.sort()
#print str(datetime.now())
print time.time()-time1
print "Sizes (Google vs VG) :",len(liste1)," ",len(liste2)

cursor1 = col2.find({'Google_Game_Name_Format':{'$exists':True}})
cursor2 = col2.find()
print cursor1.count(),' ', cursor2.count(),' ', 100*cursor1.count()/cursor2.count()

#========================================
liste1_woNbr=[game for game in liste1 if not isInt_(game.split()[-1])]
liste1_wNbr=[game for game in liste1 if isInt_(game.split()[-1])]
print "Check Sizes :",len(liste1)," : ",len(liste1_woNbr)," + ",len(liste1_wNbr)," = ",len(liste1_woNbr)+len(liste1_wNbr), " : % de nom sans numero = ",100*len(liste1_wNbr)/len(liste1),"%"

#================ Traitement game with number
'''
liste1_wNbr_noDoublon=[]
for game in liste1_wNbr:
	if not truncateExp(game) in liste1_woNbr : 
		liste1_wNbr_noDoublon.append(game)
print "% de noms avec numero et sans double (sur #noms avec numero) = ",100*len(liste1_wNbr_noDoublon)/len(liste1_wNbr),"%"
print "# de noms avec numero et sans double = ",len(liste1_wNbr_noDoublon)
print "---"

liste1_wNbr_noDoublon_Rate=['dirt 3','fifa 12','fifa 13','fifa 14','fifa 17','inazuma eleven 3','infamous 2','lego batman 2','nhl 13','nhl 14','star ocean 5','the witcher 2','wwe 12','wwe 13','grid 2'] #a la mano

liste1_wNbr_noDoublon_noRate=[]
for game in liste1_wNbr_noDoublon:
#	print game	
	if not game in liste1_wNbr_noDoublon_Rate : 
		liste1_wNbr_noDoublon_noRate.append(game)
print "% de noms avec numero et sans double sans raté (sur \#noms avec numero) = ",100*len(liste1_wNbr_noDoublon_noRate)/len(liste1_wNbr),"%"
print "# de noms avec numero et sans double sans raté  = ",len(liste1_wNbr_noDoublon_noRate)
print "Check Sizes :",len(liste1_wNbr_noDoublon_noRate)," + ",len(liste1_wNbr_noDoublon_Rate)," = ",len(liste1_wNbr_noDoublon_noRate) + len(liste1_wNbr_noDoublon_Rate)
'''

#cursor1 = col2.find({'$and':[ {'Game':'Mario Kart 7'},{'Platform':{'$exists':True}} ]})
#cursor2 = col2.find({'$and':[ {'Game':'Mario Kart 7'},{'Google Game Name Format':{'$exists':True}} ]})    
#print cursor1.count()
#print cursor2.count()

#================ Traitement game without number
'''
f=open("similarityTest_results_liste1_woNbr_cleaned.txt","r")
for line in f.readlines():
    if line == '=========\n': continue
    else :
        #print line
        lineS=line.split(', ')
        if len(lineS) == 1 : continue
        lineS[1]=lineS[1].strip()
        lineS[2]=lineS[2].strip()
        #print "Google: ", lineS[1],"VG: ",lineS[2], "length Google: ", len(lineS[1]),"length VG: ",len(lineS[2])
        col2.update_many({"Game":lineS[2]},{'$set' : {"Google_Game_Name_Format":lineS[1]}})  

f.close()
#========================================
'''
'''
f=open("similarityTest_results_liste1_woNbr.txt","w")
res=[]
res_tmp=[]
time1=time.time()
for i, x in enumerate(liste1_woNbr) :	
	#if i>0: continue
	sm.set_seq1(x)
	res_tmp=[]
	#if isInt_(x.split()[-1]): print x
	for y in liste2:
		sm.set_seq2(y)
		res_tmp.append((sm.ratio(), x, y))
	res_tmp.sort(key=itemgetter(0), reverse = True)	
	f.write('=========')	
	for elem in res_tmp[:10]:
	#	#print elem 
		f.write('\n')
		f.write(str(elem[0]))
		f.write(', ')
		f.write(str(elem[1]))
		f.write(', ')
		f.write(str(elem[2]))		
	f.write('\n')
	#if i<10: 
	#	print "==="
	#	for j, z in enumerate(res_tmp): 
	#		#if z[0]>0.5: 
	#		if j<10: print "check ",i,"eme element: ",z
	res.append(res_tmp[0])
f.close()
print time.time()-time1
'''

#liste1_wNbr_noDoublon_Rate_recup=[('dirt 3','DiRT 3'),('fifa 13','FIFA 13'),('fifa 14','FIFA 14'),('fifa 17','FIFA 17'),('inazuma eleven 3','Inazuma Eleven 3: Bomb Blast'),('inazuma eleven 3','Inazuma Eleven 3: Lightning Bolt'),('nhl 13','NHL 13'),('nhl 14','NHL 14'),('the witcher 2','The Witcher 2: Assassins of Kings'),('wwe 12','WWE \'12'),('wwe 13','WWE \'13')]

#print len(liste1_wNbr_noDoublon_Rate_recup)
#for i in range(len(liste1_wNbr_noDoublon_Rate_recup)):
#	print "Nom Google : ",liste1_wNbr_noDoublon_Rate_recup[i][0],", Nom VG : ",liste1_wNbr_noDoublon_Rate_recup[i][1]

#==================WARNING===============
#========================================

#for i in range(len(liste1_wNbr_noDoublon_Rate_recup)):
#	col2.update_many({"Game":liste1_wNbr_noDoublon_Rate_recup[i][1]},{"$set" : {"Google_Game_Name_Format":liste1_wNbr_noDoublon_Rate_recup[i][0]}})

'''
print "lengh of entries to update : ",len(res)
for i in range(len(res)):
	print "Nom Google : ",res[i][1],", Nom VG : ",res[i][2]
for i in range(len(res)):
	col2.update_many({"Game":res[i][2]},{'$set' : {"Google_Game_Name_Format":res[i][1]}})
'''

#========================================
#========================================

#res.sort(key=itemgetter(0), reverse = True)
# or : data.sort(key=lambda tup: tup[1])  #but : itemgetter class sorts 126% faster on average than the equivalent lambda function

'''
goodRes=[]
badRes=[]
for resultat in res:
	if resultat[0]>0.5: goodRes.append(resultat)
	else: badRes.append(resultat)

for result in goodRes: print "good result : ", result
for result in badRes: print "bad result : ", result
print "% good result = ", 100*len(goodRes)/len(res)
'''

#for game1, game2, in itertools.izip(liste1, liste2):
#	print "GooleTrend :", game1, ", Vg :", game2
	


