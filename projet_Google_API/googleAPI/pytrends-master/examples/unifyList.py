from pytrends.request import TrendReq
import json
import pymongo                          
from pymongo import MongoClient

client = MongoClient('84.39.39.108')
db = client.test_db
col=db.ref_list_gameCleanMinus_unique
listeGame = col.find_one()['Game']
listeGameClean=[]
dict_={}

def unifyList(seq): # Order preserving
  #Modified version of Dave Kirby solution
  seen = set()
  return [x for x in seq if x not in seen and not seen.add(x)]

listeGameClean=unifyList(listeGame)

dict_["Game"]=listeGameClean
colout = db.ref_list_gameCleanMinus_unique_test
post_id = colout.insert_one(dict_).inserted_id
